from django.db import models

from balances.models import DEFAULT_MONEY
from core.models import TimeStampedModel
from exchanges.models import UserExchange
from exchanges.utils import get_current_value

BUY_SIDE = 'buy'
SELL_SIDE = 'sell'


class TradeExchangeSync(TimeStampedModel):
    user_exchange = models.ForeignKey(UserExchange, on_delete=models.CASCADE)


# a = {
#     'id': '5053303',
#     'timestamp': 1517070118702,
#     'datetime': '2018-01-27T16:21:59.702Z',
#     'symbol': 'LTC/ETH',
#     'type': 'market',
#     'side': 'sell',
#     'price': 0.0,
#     'amount': 0.648,
#     'cost': 0.0,
#     'filled': 0.648,
#     'remaining': 0.0,
#     'status': 'closed',
#     'fee': None
# }


class Trade(TimeStampedModel):
    trade_sync = models.ForeignKey('TradeExchangeSync', on_delete=models.CASCADE, null=True, blank=True)
    identifier = models.CharField(max_length=512)
    identifier_exchange_code = models.CharField(max_length=512)
    date = models.DateTimeField()
    timestamp = models.IntegerField(null=True, blank=True)
    symbol = models.CharField(max_length=32)
    type = models.CharField(max_length=32, null=True, blank=True)
    side = models.CharField(max_length=32)
    price = models.DecimalField(max_digits=64, decimal_places=10, default=0)
    amount = models.DecimalField(max_digits=64, decimal_places=10, default=0)
    cost = models.DecimalField(max_digits=64, decimal_places=10, default=0)
    filled = models.DecimalField(max_digits=64, decimal_places=10, default=0)
    remaining = models.DecimalField(max_digits=64, decimal_places=10, default=0)
    status = models.CharField(max_length=32)
    fee_cost = models.DecimalField(max_digits=64, decimal_places=10, default=0)
    fee_currency = models.CharField(max_length=32, null=True, blank=True)
    timestamped_unitary_usd_value = models.DecimalField(max_digits=64, decimal_places=10, null=True, blank=True)

    class Meta:
        unique_together = (('identifier', 'identifier_exchange_code'),)
        ordering = ['-date']

    def __str__(self):
        return '{} {} {} for {} {} - {}'.format(self.side, self.amount, self.left_symbol(), self.cost, self.right_symbol(), self.date.strftime("%Y-%m-%d %H:%M"))

    def left_symbol(self):
        return self.symbol.split('/')[0]

    def right_symbol(self):
        return self.symbol.split('/')[1]

    def timestamped_total_usd_value(self):
        if self.status == 'open':
            value, money = get_current_value(self.amount, self.left_symbol(), 'USD')
        else:
            if self.timestamped_unitary_usd_value:
                value, money = self.amount * self.timestamped_unitary_usd_value, 'USD'
            else:
                value, money = 0, 'USD'
        return value, money

    def timestamped_total_value(self, money=DEFAULT_MONEY):
        timestamped_total_usd_quantity, timestamped_total_usd_money = self.timestamped_total_usd_value()
        current_value, money = get_current_value(timestamped_total_usd_quantity, timestamped_total_usd_money, money)
        return current_value, money


        # identifier = models.CharField(max_length=512, unique=True)
        # buyed_price_usd = models.DecimalField(max_digits=64, decimal_places=10, null=True, blank=True)
        # sold_price_usd = models.DecimalField(max_digits=64, decimal_places=10, null=True, blank=True)
        # buyed_quantity = models.DecimalField(max_digits=64, decimal_places=10, )
        # buyed_money = models.CharField(max_length=32)
        # sold_price = models.DecimalField(max_digits=64, decimal_places=10, null=True, blank=True)
        # sold_quantity = models.DecimalField(max_digits=64, decimal_places=10, )
        # sold_money = models.CharField(max_length=32)
        #
        # fee_quantity = models.DecimalField(max_digits=64, decimal_places=10, null=True, blank=True)
        # fee_money = models.CharField(max_length=32, null=True, blank=True)
        # date = models.DateTimeField()

        # def __unicode__(self):
        #     return '+ {} {} / - {} {}'.format(self.buyed_quantity, self.buyed_money, self.sold_quantity, self.sold_money)
        #
        # def buyed_value(self, money=DEFAULT_MONEY, exchange_code=None):
        #     if exchange_code is None:
        #         exchange_code = self.trade_sync.user_exchange.exchange.code
        #     current_value = get_current_value(self.buyed_quantity, self.buyed_money, money, exchange_code=exchange_code)
        #     return current_value
        #
        # def buyed_object(self):
        #     return self.buyed_quantity, self.buyed_money
        #
        # def sold_value(self, money=DEFAULT_MONEY, exchange_code=None):
        #     if exchange_code is None:
        #         exchange_code = self.trade_sync.user_exchange.exchange.code
        #     current_value = get_current_value(self.sold_quantity, self.sold_money, money, exchange_code=exchange_code)
        #     return current_value
        #
        # def sold_object(self):
        #     return self.sold_quantity, self.sold_money
