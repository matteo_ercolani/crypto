import time

from exchanges.utils import init_exchange_api
from trades.models import Trade, TradeExchangeSync


def sync_trades(user):
    exchanges = init_exchange_api(user)

    for exchange in exchanges:
        sync = TradeExchangeSync.objects.create(user_exchange=exchange['user_exchange'])
        sync_trades_exchange(exchange['user_exchange_api'], exchange['user_exchange'], trade_sync=sync)


def sync_trades_exchange(user_exchange_api, user_exchange, trade_sync=None):
    last_balance = user_exchange.last_balance()
    # last_balance_object = last_balance.object()
    last_balance_symbols_list = last_balance.get_symbols_list()
    exchange_code = user_exchange.exchange.code

    for symbol in last_balance_symbols_list:

        data = user_exchange_api.fetch_orders(symbol)

        for trade in data:

            fee_data = trade['fee']
            if not fee_data:
                fee_data = {}

            Trade.objects.update_or_create(
                trade_sync=trade_sync,
                identifier=trade['id'],
                identifier_exchange_code=exchange_code,
                date=trade['datetime'],
                timestamp=trade['timestamp'],
                symbol=trade['symbol'],
                type=trade['type'],
                side=trade['side'],
                price=trade['price'],
                amount=trade['amount'],
                cost=trade['cost'],
                filled=trade['filled'],
                remaining=trade['remaining'],
                status=trade['status'],
                fee_cost=fee_data.get('fee_cost', 0),
                fee_currency=fee_data.get('fee_currency'),
            )

        time.sleep(3)

    print('Fatto')


a = {'id': '5053303', 'timestamp': 1517070118702,
     'datetime': '2018-01-27T16:21:59.702Z', 'symbol': 'LTC/ETH', 'type': 'market', 'side': 'sell', 'price': 0.0,
     'amount': 0.648, 'cost': 0.0, 'filled': 0.648, 'remaining': 0.0, 'status': 'closed', 'fee': None}
