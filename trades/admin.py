from django.contrib import admin

from .models import TradeExchangeSync, Trade


class TradeInline(admin.TabularInline):
    model = Trade
    extra = 0


class TradeExchangeSyncAdmin(admin.ModelAdmin):
    inlines = [
        TradeInline,
    ]


class TradeAdmin(admin.ModelAdmin):
    model = Trade

    list_display = [
        'symbol',
        'side',
        'amount',
        'cost',
        'date',
        'status',
    ]

    list_filter = [
        'side',
        'symbol',
        'status',
    ]


admin.site.register(TradeExchangeSync, TradeExchangeSyncAdmin)
admin.site.register(Trade, TradeAdmin)
