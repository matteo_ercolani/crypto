from decimal import Decimal

import requests
from django.core.cache import cache

MONEYS = (
    ('EUR', 'EUR'),
    ('USD', 'USD'),
    ('BTC', 'BTC'),
    ('BCH', 'BCH'),
    ('LTC', 'LTC'),
    ('ETH', 'ETH'),
    ('XRP', 'XRP'),
    ('IOT', 'IOTA'),
    ('ETC', 'ETC'),
    ('XMR', 'XMR'),
    ('TRX', 'TRX'),
    ('XLM', 'XLM'),
)

FIAT_MONEYS = (
    'EUR',
    'USD',
)

FIAT_CACHE = 3600 * 12


def usd_to_eur(usd):
    cache_key = 'USD_EUR'
    rate = cache.get(cache_key)
    if rate is None:
        url = 'http://data.fixer.io/api/latest?access_key=edf7adb9622a55c39dfc2724e04f21f4&symbols=EUR,USD'
        response = requests.request("GET", url)
        response.raise_for_status()
        result = response.json()
        rate = result['rates']['USD']
        cache.set(cache_key, rate, FIAT_CACHE)
    return Decimal(usd) / Decimal(rate)


def fiat_money_converter(value, fiat_from, fiat_to):
    fiat_to = fiat_to.upper()
    fiat_from = fiat_from.upper()
    if fiat_from == fiat_to:
        return value, fiat_to

    cache_key = '{}_{}'.format(fiat_from, fiat_to)
    rate = cache.get(cache_key)
    if rate is None:
        url = 'http://data.fixer.io/api/latest?access_key=edf7adb9622a55c39dfc2724e04f21f4&symbols={},{}'.format(fiat_to, fiat_from)
        response = requests.request("GET", url)
        response.raise_for_status()
        result = response.json()
        rate = result['rates'][fiat_from]
        cache.set(cache_key, rate, FIAT_CACHE)
    return Decimal(value) / Decimal(rate), fiat_to

# def calculate_eur_value(quantity, money):
#     if money == 'iota':
#         money = 'iot'
#     if money == 'eur':
#         total_value = quantity
#
#     elif money == 'usd':
#         total_value = usd_to_eur(quantity)
#
#     elif money in ['btc', 'eth']:
#         try:
#             eur_value = get_money_current_value(money, 'eur')
#         except HTTPError:
#             usd_value = get_money_current_value(money, 'usd')
#             eur_value = usd_to_eur(usd_value)
#
#         total_value = quantity * Decimal(eur_value)
#
#     else:
#         usd_value = get_money_current_value(money, 'usd')
#         eur_value = usd_to_eur(usd_value)
#         total_value = quantity * Decimal(eur_value)
#     return total_value
