from django.urls import path

from .views import DepositListView, DepositCreateView, DepositUpdateView, DepositDeleteView

urlpatterns = [
    path('deposits/', DepositListView.as_view(), name='deposit-list'),
    path('deposits/create/', DepositCreateView.as_view(), name='deposit-create'),
    path('deposits/<int:pk>/update/', DepositUpdateView.as_view(), name='deposit-update'),
    path('deposits/<int:pk>/delete/', DepositDeleteView.as_view(), name='deposit-delete'),
]