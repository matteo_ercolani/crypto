from django.conf import settings

from deposits.models import Deposit
from exchanges.utils import get_current_value


def calculate_deposit_values(user):
    deposited = {}

    for deposit in Deposit.objects.filter(user=user):

        deposit_quantity, deposit_money = deposit.deposit_value
        try:
            deposited[deposit_money]
            deposited[deposit_money] += deposit_quantity
        except KeyError:
            deposited[deposit_money] = deposit_quantity

    return deposited


def calculate_deposit(user, fiat_money=settings.DEFAULT_FIAT_MONEY):
    value = 0
    deposit = calculate_deposit_values(user)

    for money, quantity in deposit.items():
        value += get_current_value(quantity, money, fiat_money)[0]

    return value, fiat_money


def calculate_eur_deposit(user):
    currency = 'EUR'
    return calculate_deposit(user, fiat_money=currency)
