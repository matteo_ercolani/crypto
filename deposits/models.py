from django.conf import settings
from django.db import models

# Create your models here.
from core.models import TimeStampedModel
from core.utils import MONEYS
from exchanges.models import Exchange


class Deposit(TimeStampedModel):
    exchange = models.ForeignKey(Exchange, on_delete=models.CASCADE)
    quantity = models.DecimalField(max_digits=64, decimal_places=10)
    money = models.CharField(choices=MONEYS, max_length=32)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    date = models.DateTimeField()

    def __unicode__(self):
        return '{} {}'.format(self.quantity, self.money)

    def __str__(self):
        return '{} {}'.format(self.quantity, self.money)

    @property
    def deposit_value(self):
        return self.quantity, self.money
