from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from deposits.utils import calculate_eur_deposit
from .models import Deposit


class DepositListView(ListView):
    model = Deposit

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        total_deposit_eur_quantity, total_deposit_eur_money = calculate_eur_deposit(self.request.user)
        context['total_deposit_eur'] = {'quantity': round(total_deposit_eur_quantity, 2), 'money': total_deposit_eur_money}
        return context

    def get_queryset(self):
        return Deposit.objects.filter(user=self.request.user)


class DepositCreateView(LoginRequiredMixin, CreateView):
    model = Deposit
    fields = ['exchange', 'quantity', 'money', 'date']
    success_url = reverse_lazy('deposit-list')
    login_required = True

    def get_initial(self):
        return {'date': datetime.now()}

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class DepositUpdateView(LoginRequiredMixin, UpdateView):
    model = Deposit
    fields = ['exchange', 'quantity', 'money', 'date']
    success_url = reverse_lazy('deposit-list')

    def get_queryset(self):
        return Deposit.objects.filter(user=self.request.user)


class DepositDeleteView(LoginRequiredMixin, DeleteView):
    model = Deposit
    success_url = reverse_lazy('deposit-list')

    def get_queryset(self):
        return Deposit.objects.filter(user=self.request.user)
