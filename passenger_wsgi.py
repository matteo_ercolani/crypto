import os
import sys

try:
    if os.environ['CURRENT_ENV'] == 'LIVE':
        CURRENT_ENV = 'LIVE'
        DEBUG = False
    elif os.environ['CURRENT_ENV'] == 'DEVELOP':
        CURRENT_ENV = 'DEVELOP'
    else:
        CURRENT_ENV = 'DEVELOP'
except KeyError:
    CURRENT_ENV = 'LOCAL'


if CURRENT_ENV == 'LIVE':
    env_name = 'crypto_live_env'
else:
    env_name = 'crypto_develop_env'

app_name = 'CRYPTO'

env_path = os.path.dirname(os.getcwd())
cwd = os.getcwd()
sys.path.append(cwd)
sys.path.append(cwd + '/' + app_name)

INTERP = env_path + '/' + env_name + '/bin/python'

if sys.executable != INTERP: os.execl(INTERP, INTERP, *sys.argv)
sys.path.insert(0, env_path + '/' + env_name + '/bin')
# sys.path.insert(0, env_path + '/' + env_name + '/lib/python3.5/site-packages/django')
sys.path.insert(0, env_path + '/' + env_name + '/lib/python3.5/site-packages')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", app_name + ".settings")
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
