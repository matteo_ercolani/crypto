from django.urls import path

from . import views

urlpatterns = [
    path('balances/', views.balances, name='balances'),
    path('sync-balance/', views.sync_user_balance, name='sync_user_balance'),
    path('batch-balances-logs/', views.sync_balances_for_all_users, name='batch_balances_logs'),
]
