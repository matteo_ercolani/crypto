from django.db.models.signals import post_save
from django.dispatch import receiver

from balances.utils import sync_balances
from exchanges.models import UserExchange


@receiver(post_save, sender=UserExchange)
def save_profile(sender, instance, **kwargs):
    try:
        sync_balances(instance.user)
    except Exception as exc:
        pass
