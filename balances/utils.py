from decimal import Decimal

from django.conf import settings

from balances.models import BalanceExchangeSync, CoinBalance
from exchanges.utils import init_exchange_api, get_current_value, get_current_value_rate


def last_total_balance(user):
    total_balance = {}

    for user_exchange in user.userexchange_set.all():
        exchange_balance_data = user_exchange.last_balance_object()

        for money, quantity in exchange_balance_data.items():
            try:
                total_balance[money]
                total_balance[money] += quantity
            except KeyError:
                total_balance[money] = quantity

    return total_balance


def last_total_balance_with_fiat(fiat_money, user):
    currencies = []

    for money, quantity in last_total_balance(user).items():
        fiat_unit_quantity, fiat_unit_money = get_current_value_rate(money, fiat_money)
        fiat_quantity, fiat_money = get_current_value(quantity, money, fiat_money)
        currency = {
            'money': money,
            'quantity': quantity,
            'unit_value_fiat': {'quantity': round(fiat_unit_quantity, 2), 'money': fiat_unit_money},
            'total_value_fiat': {'quantity': round(fiat_quantity, 2), 'money': fiat_money},
        }
        currencies.append(currency)

    return currencies


def last_total_balance_eur(user):
    total_balance = {}

    for money, quantity in last_total_balance(user).items():
        total_balance[money] = get_current_value(quantity, money, 'EUR')[0]

    return total_balance


def last_total_balance_value_eur(user):
    return last_total_balance_value(user, 'EUR')


def last_total_balance_value(user, money=settings.DEFAULT_FIAT_MONEY):
    value = 0
    for user_exchange in user.userexchange_set.all():
        value += user_exchange.last_balance_value(money=money)[0]

    return value, money


def sync_balances(user):
    exchanges = init_exchange_api(user)

    for exchange in exchanges:
        sync_balance_exchange(exchange['user_exchange_api'], exchange['user_exchange'])


def sync_balance_exchange(user_exchange_api, user_exchange):
    data = user_exchange_api.fetch_balance()

    sync = BalanceExchangeSync.objects.create(user_exchange=user_exchange)

    for currency, quantity in data['total'].items():

        if quantity > 0:
            CoinBalance.objects.create(money=currency.lower(), quantity=Decimal(quantity), balance_sync=sync)
