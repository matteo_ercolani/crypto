from django.apps import AppConfig
from django.db.models.signals import post_save
from django.dispatch import receiver


class BalancesConfig(AppConfig):
    name = 'balances'

    def ready(self):
        import balances.signals
