from django.contrib import admin

from .models import BalanceExchangeSync, CoinBalance, UserBalanceLog


class CoinBalanceInline(admin.TabularInline):
    model = CoinBalance
    extra = 0


class BalanceExchangeSyncAdmin(admin.ModelAdmin):
    inlines = [
        CoinBalanceInline,
    ]


class UserBalanceLogAdmin(admin.ModelAdmin):
    fields = [
        'user',
        'quantity',
        'money',
    ]
    list_display = [
        'created',
        'user',
        'quantity',
        'money',
    ]
    list_filter = [
        'created',
    ]


admin.site.register(BalanceExchangeSync, BalanceExchangeSyncAdmin)
admin.site.register(UserBalanceLog, UserBalanceLogAdmin)
