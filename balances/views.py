import datetime

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect

from accounts.models import User
from balances.models import UserBalanceLog
from balances.utils import last_total_balance_with_fiat, last_total_balance_value_eur, sync_balances, \
    last_total_balance_value
from exchanges.models import UserExchange
from exchanges.utils import get_current_value, get_current_value_rate


def get_balances_for_exchanges(user, fiat_money='EUR'):
    user_exchanges = UserExchange.objects.filter(user=user)

    exchanges_data = []

    for user_exchange in user_exchanges:

        exchange_data = {
            'name': user_exchange.exchange.name,
            'code': user_exchange.exchange.code,
            'last_sync': user_exchange.created
        }
        exchange_code = user_exchange.exchange.code
        exchange_fiat_value = 0
        deposits_data = {}

        last_balance = user_exchange.last_balance_object()

        for money, quantity in last_balance.items():
            fiat_quantity, fiat_money = get_current_value(quantity, money, fiat_money, exchange_code=exchange_code)
            exchange_fiat_value += fiat_quantity
            fiat_unit_quantity, fiat_unit_money = get_current_value_rate(money, fiat_money, exchange_code=exchange_code)

            deposits_data[money] = {
                'quantity': quantity,
                'unit_value_fiat': {'quantity': round(fiat_unit_quantity, 2), 'money': fiat_unit_money},
                'total_value_fiat': {'quantity': round(fiat_quantity, 2), 'money': fiat_money},
            }

        exchange_fiat_value = round(exchange_fiat_value, 2)

        exchange_data['deposits'] = deposits_data
        exchange_data['total_deposit_fiat'] = {'quantity': exchange_fiat_value, 'money': fiat_money}

        exchanges_data.append(exchange_data)

    return exchanges_data


@login_required
def balances(request):
    currency = request.GET.get('currency', 'EUR')

    context = {}
    context['exchanges_data'] = get_balances_for_exchanges(request.user, fiat_money=currency)
    context['total_currencies_data'] = last_total_balance_with_fiat(currency, request.user)

    import json


    def myconverter(o):
        if isinstance(o, datetime.datetime):
            return o.__str__()

    a = json.dumps(context['exchanges_data'], default=myconverter)
    print(a)

    total_balance_quantity, total_balance_money = last_total_balance_value(request.user, currency)
    context['total_balance'] = {'quantity': round(total_balance_quantity, 2), 'money': total_balance_money}

    return render(request, 'balances/balances.html', context)


@login_required
def sync_user_balance(request):
    sync_balances(request.user)
    return redirect('balances')


def sync_balances_for_all_users(request):
    token = request.GET.get('token')

    if token == 'asdidfnsivbskvb':
        for user in User.objects.all():
            balance_quantity, balance_money = last_total_balance_value_eur(user)
            if balance_quantity:
                UserBalanceLog.objects.create(quantity=balance_quantity, money=balance_money, user=user)
        response = 'ok'
        status_code = 200
    else:
        response = 'ko'
        status_code = 500

    return HttpResponse(response, status=status_code)
