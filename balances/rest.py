from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from balances.serializers import BalanceExchangeSyncSerializer
from .models import BalanceExchangeSync


class BalanceExchangeSyncViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing the accounts
    associated with the user.
    """
    serializer_class = BalanceExchangeSyncSerializer
    queryset = BalanceExchangeSync.objects.all()
    permission_classes = (AllowAny,)
