from rest_framework import serializers

from .models import BalanceExchangeSync


class BalanceExchangeSyncSerializer(serializers.ModelSerializer):
    class Meta:
        model = BalanceExchangeSync
        # fields = (
        #     'exchange',
        #     'buy_quantity',
        #     'buy_money',
        #     'sell_quantity',
        #     'sell_money',
        #     'date',
        #     'calculate_gain',
        #     'eur_value',
        # )
