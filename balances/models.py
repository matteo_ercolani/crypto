from django.conf import settings
from django.db import models

from core.models import TimeStampedModel
from exchanges.models import UserExchange
from exchanges.utils import get_current_value

DEFAULT_MONEY = 'USD'


class CoinBalance(TimeStampedModel):
    balance_sync = models.ForeignKey('BalanceExchangeSync', on_delete=models.CASCADE)
    quantity = models.DecimalField(max_digits=64, decimal_places=10, )
    money = models.CharField(max_length=32)

    def value(self, money=DEFAULT_MONEY, exchange_code=None):
        if exchange_code is None:
            exchange_code = self.balance_sync.user_exchange.exchange.code
        current_value = get_current_value(self.quantity, self.money, money, exchange_code=exchange_code)
        return current_value

    def object(self):
        return self.quantity, self.money


class BalanceExchangeSync(TimeStampedModel):
    user_exchange = models.ForeignKey(UserExchange, on_delete=models.CASCADE)

    def value(self, money=DEFAULT_MONEY, exchange_code=None):
        result = 0
        for coin in self.coinbalance_set.all():
            value, value_money = coin.value(money, exchange_code=exchange_code)
            result += value
        return result, money

    def object(self):
        result = {}
        for coin in self.coinbalance_set.all():
            result[coin.money] = coin.quantity
        return result

    def get_symbols_list(self):
        balance_symbols_list = []

        available_symbols = self.user_exchange.exchange.symbols()
        hold_moneys = list(self.coinbalance_set.all().values_list('money', flat=True))

        for money in hold_moneys:
            for symbol in available_symbols:
                if money.upper() == symbol.split('/')[0]:
                    balance_symbols_list.append(symbol)

        return balance_symbols_list


class UserBalanceLog(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    quantity = models.DecimalField(max_digits=64, decimal_places=10)
    money = models.CharField(max_length=8)

    def __str__(self):
        return '{}: {} {}'.format(self.user.email, str(self.quantity), self.money)
