from django.urls import path

from .views import OperationListView, OperationCreateView, OperationUpdateView, OperationDeleteView, \
    OperationUpdateStep2View

urlpatterns = [
    path('operations/', OperationListView.as_view(), name='operation-list'),
    path('operations/create/', OperationCreateView.as_view(), name='operation-create'),
    path('operations/<int:pk>/step2/', OperationUpdateStep2View.as_view(), name='operation-step2'),
    path('operations/<int:pk>/update/', OperationUpdateView.as_view(), name='operation-update'),
    path('operations/<int:pk>/delete/', OperationDeleteView.as_view(), name='operation-delete'),
]