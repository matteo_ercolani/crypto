from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from balances.models import DEFAULT_MONEY
from core.models import TimeStampedModel
from core.utils import FIAT_MONEYS
from exchanges.models import UserExchange
from exchanges.utils import get_current_value
from trades.models import Trade, BUY_SIDE, SELL_SIDE


class Operation(TimeStampedModel):
    user_exchange = models.ForeignKey(UserExchange, on_delete=models.CASCADE)
    opened_trade = models.ForeignKey(Trade, on_delete=models.CASCADE, related_name='open_trade', null=True, blank=True)
    closed_trade = models.ForeignKey(Trade, on_delete=models.CASCADE, related_name='closed_trade', null=True,
                                     blank=True)

    def opened_quantity(self):
        quantity = 0

        if self.opened_trade:
            if self.opened_trade.side == BUY_SIDE:
                quantity = self.opened_trade.cost
            else:
                quantity = self.opened_trade.amount

        return quantity

    def closed_quantity(self):
        quantity = 0

        if self.closed_trade:
            if self.opened_trade.side == BUY_SIDE:
                quantity = self.closed_trade.cost
            else:
                quantity = self.closed_trade.amount

        return quantity

    def opened_money(self):
        money = None
        if self.opened_trade:
            if self.opened_trade.side == BUY_SIDE:
                money = self.opened_trade.right_symbol()
            else:
                money = self.opened_trade.left_symbol()
        return money

    def closed_money(self):
        money = None
        if self.closed_trade:
            if self.closed_trade.side == SELL_SIDE:
                money = self.closed_trade.right_symbol()
            else:
                money = self.closed_trade.left_symbol()
        return money

    def opened_object(self):
        return {
            'quantity': self.opened_quantity(),
            'money': self.opened_money(),
        }

    def closed_object(self):
        return {
            'quantity': self.closed_quantity(),
            'money': self.closed_money(),
        }

    def opened_object_string(self):
        obj = self.opened_object()
        return '{} {}'.format(obj['quantity'], obj['money'])

    def closed_object_string(self):
        obj = self.closed_object()
        return '{} {}'.format(obj['quantity'], obj['money'])

    def current_opened_value(self, money=DEFAULT_MONEY, exchange_code=None):
        if exchange_code is None:
            exchange_code = self.user_exchange.exchange.code
        current_value, money = get_current_value(self.opened_quantity(), self.opened_money(), money,
                                                 exchange_code=exchange_code)
        return round(current_value, 2), money

    def current_opened_value_string(self, money=DEFAULT_MONEY, exchange_code=None):
        current_value, current_money = self.current_opened_value(money=money, exchange_code=exchange_code)
        return '{} {}'.format(current_value, current_money)

    def current_closed_value(self, money=DEFAULT_MONEY, exchange_code=None):
        if exchange_code is None:
            exchange_code = self.user_exchange.exchange.code
        current_value, money = get_current_value(self.closed_quantity(), self.closed_money(), money,
                                                 exchange_code=exchange_code)
        return round(current_value, 2), money

    def current_closed_value_string(self, money=DEFAULT_MONEY, exchange_code=None):
        current_value, current_money = self.current_closed_value(money=money, exchange_code=exchange_code)
        return '{} {}'.format(current_value, current_money)

    @property
    def current_opened_usd_value_string(self):
        return self.current_opened_value_string(money='USD')

    @property
    def current_closed_usd_value_string(self):
        return self.current_closed_value_string(money='USD')

    def timestamped_usd_gain_value(self):
        gain = 0
        opened_total_usd, money = self.opened_trade.timestamped_total_usd_value()
        closed_total_usd, money = self.closed_trade.timestamped_total_usd_value()

        if opened_total_usd and closed_total_usd:
            gain = closed_total_usd - opened_total_usd

        return gain, 'USD'

    def gain_value(self):
        if self.opened_trade.side == 'buy':
            quantity, money = self.timestamped_usd_gain_value()
        else:
            quantity = self.closed_trade.amount - self.opened_trade.amount
            money = self.opened_trade.left_symbol()
        return quantity, money

    def gain_value_string(self):
        try:
            quantity, money = self.gain_value()
            if money in FIAT_MONEYS:
                quantity = round(quantity, 2)
            return '{} {}'.format(quantity, money)
        except AttributeError:
            return 'Unknown'

    def gain_value_object(self):
        quantity, money = self.gain_value()
        if money in FIAT_MONEYS:
            quantity = round(quantity, 2)
        return {
            'quantity': quantity,
            'money': money
        }

    @property
    def timestamped_usd_gain_value_string(self):
        current_value, current_money = self.timestamped_usd_gain_value()
        return '{} {}'.format(round(current_value, 2), current_money)

    def clean(self):
        # Don't allow draft entries to have a pub_date.
        if self.closed_trade:
            if self.opened_trade.symbol != self.closed_trade.symbol:
                raise ValidationError(_('Open trade symbol and closed trade symbols must be the same.'))
