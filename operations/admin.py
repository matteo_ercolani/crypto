from django.contrib import admin

from .models import Operation


class OperationAdmin(admin.ModelAdmin):
    model = Operation

    list_display = [
        'opened_trade',
        'closed_trade',
        'opened_object_string',
        'closed_object_string',
        'current_opened_usd_value_string',
        'current_closed_usd_value_string',
        'gain_value_string',
    ]
    readonly_fields = [
        'opened_object_string',
        'closed_object_string',
        'current_opened_usd_value_string',
        'current_closed_usd_value_string',
        'gain_value_string',
    ]


admin.site.register(Operation, OperationAdmin)
