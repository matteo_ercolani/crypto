from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from trades.models import Trade
from .models import Operation


class OperationListView(ListView):
    model = Operation

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # total_deposit_eur_quantity, total_deposit_eur_money = calculate_eur_deposit(self.request.user)
        # context['total_deposit_eur'] = {'quantity': round(total_deposit_eur_quantity, 2), 'money': total_deposit_eur_money}
        return context

    def get_queryset(self):
        return Operation.objects.filter(user_exchange__user=self.request.user)


class OperationCreateView(LoginRequiredMixin, CreateView):
    model = Operation
    fields = ['opened_trade']
    success_url = reverse_lazy('operation-list')
    login_required = True

    def get_initial(self):
        return {'date': datetime.now()}

    def form_valid(self, form):
        form.instance.user_exchange = form.instance.opened_trade.trade_sync.user_exchange
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('operation-step2', args=[self.object.pk])


class OperationUpdateStep2View(LoginRequiredMixin, UpdateView):
    model = Operation
    fields = ['closed_trade']
    success_url = reverse_lazy('operation-list')

    def get_queryset(self):
        return Operation.objects.filter(user_exchange__user=self.request.user)

    def get_form(self, form_class=None):
        opened_trade = self.object.opened_trade
        trade_sync = opened_trade.trade_sync

        form = super().get_form(form_class)
        form.fields['closed_trade'].queryset = Trade.objects.filter(trade_sync=trade_sync, date__gte=opened_trade.date, symbol=opened_trade.symbol).exclude(pk=opened_trade.pk)
        return form
        # self.fields['trade'].queryset = Rate.objects.filter(company=company)

    def get_template_names(self):
        return 'operations/operation_form_step2.html'


class OperationUpdateView(LoginRequiredMixin, UpdateView):
    model = Operation
    fields = ['exchange', 'quantity', 'money', 'date']
    success_url = reverse_lazy('operation-list')

    def get_queryset(self):
        return Operation.objects.filter(user_exchange__user=self.request.user)


class OperationDeleteView(LoginRequiredMixin, DeleteView):
    model = Operation
    success_url = reverse_lazy('operation-list')

    def get_queryset(self):
        return Operation.objects.filter(user_exchange__user=self.request.user)
