# progetto dentro plesk


## sezione WebSite & Domains:

  - creare il progetto


## sezione WebSite & Domains --> nome progetto --> host settings:

  - creare il progetto e far puntare a /application
  - abilitare il supporto a python e disabilitare: php, cgi, fastcgi


## sezione WebSite & Domains --> nome progetto --> web hosting access:

  - abilitare l utente al demone ssh (/bin/bash)


## ssh

  - eliminare il contenuto di /application
  - creare la virtualenv ```virtualenv -p python3 crypto_env``` nella root del progetto (che corrisponde alla home dell user)


## sezione WebSite & Domains --> nome progetto --> git:

  - abilitare il modulo git (ora punterà alla cartella /application)
  - eventualmente aggiungere la chiave dentro bitbucket od altro

## sezione WebSite & Domains --> nome progetto --> host settings:

  - modifcare il puntamento alla cartella degli statitc (es: application/staticfiles/ )

## sezione WebSite & Domains --> nome progetto --> Apache & nginx Settings:

  - deselezionare Proxy mode (sezione nginx settings)
  - in Apache & nginx Settings, aggiungere la regola:

    ```
      passenger_enabled on;
      passenger_python /var/www/vhosts/crypto.waydotnet.com/crypto_env/bin/python;
    ```
    ovviamente modifcare i percorsi

## progetto django:

  - aggiungere il file **passenger_wsgi.py** con il seguente contenuto:


  ```
  import sys, os

  app_name = 'CRYPTO'
  env_name = 'crypto_env'

  env_path = os.path.dirname(os.getcwd())
  cwd = os.getcwd()
  sys.path.append(cwd)
  sys.path.append(cwd + '/' + app_name)

  INTERP = env_path + '/' + env_name + '/bin/python'

  if sys.executable != INTERP: os.execl(INTERP, INTERP, *sys.argv)
  sys.path.insert(0, env_path + '/' + env_name + '/bin')
  #sys.path.insert(0, env_path + '/' + env_name + '/lib/python3.5/site-packages/django')
  sys.path.insert(0, env_path + '/' + env_name + '/lib/python3.5/site-packages')
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", app_name + ".settings")
  from django.core.wsgi import get_wsgi_application
  application = get_wsgi_application()

  ```

  modificare i nomi di app_name ed env_name


## bitbucket

  - abilitare il webhook



#documentazione:

 - https://www.plesk.com/blog/product-technology/plesk-and-django/
 - https://support.plesk.com/hc/en-us/articles/115002701209-How-to-configure-Django-application-in-Plesk-
 - https://medium.com/@thehorsetechie/securing-django-site-with-plesk-using-lets-encrypt-aa8b1998ba54 (non ho idea se serve o meno)

