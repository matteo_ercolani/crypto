from decimal import Decimal

from django.contrib.auth.decorators import login_required
from django.shortcuts import render

# @cache_page(60 * 60 * 2)
from balances.utils import last_total_balance_value
from balances.views import get_balances_for_exchanges
from charts.rest import last_day_hours_chart, last_hour_chart
from deposits.utils import calculate_deposit


@login_required
def home(request):
    currency = request.GET.get('currency', 'EUR')
    total_balance_quantity, total_balance_money = last_total_balance_value(request.user, currency)
    total_balance = {'quantity': round(total_balance_quantity, 2), 'money': total_balance_money}

    total_deposit_quantity, total_deposit_money = calculate_deposit(request.user, currency)
    total_deposit = {'quantity': round(total_deposit_quantity, 2), 'money': total_balance_money}

    total_gain_quantity = round(total_balance_quantity - total_deposit_quantity, 2)
    total_gain = {'quantity': round(total_gain_quantity, 2), 'money': currency}

    if total_deposit_quantity > 0:
        total_gain_percent = round(total_balance_quantity / total_deposit_quantity * 100, 2)
    else:
        total_gain_percent = None

    last_day_balance_data = last_day_hours_chart(request.user)
    last_hour_balance_data = last_hour_chart(request.user)

    exchanges_data = get_balances_for_exchanges(request.user, fiat_money=currency)

    context = {
        'portfolio': {
            'total_balance': total_balance,
            'total_deposit': total_deposit,
            'total_gain': total_gain,
            'total_gain_percent': total_gain_percent,
        },
        'charts': {
            'last_day_balance': last_day_balance_data,
            'last_hour_balance': last_hour_balance_data,
        },
        'exchanges_data': exchanges_data,
    }

    return render(request, 'dashboard/home/home.html', context)
