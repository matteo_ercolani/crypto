type = ['', 'info', 'success', 'warning', 'danger'];


demo = {

  initDashboardPageCharts: function () {

    /* ----------==========     Daily Sales Chart initialization    ==========---------- */

    dataLastDayBalanceChart = {
      labels: dataLastDayBalanceLabels,
      series: [
        dataLastDaySeries
      ]
    };

    optionsLastDayBalanceChart = {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0
      }),
      low: dataLastDaySeriesMin,
      high: dataLastDaySeriesMax, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
      chartPadding: {top: 0, right: 0, bottom: 0, left: 0}
    };

    var lastDayBalanceChart = new Chartist.Line('#lastDayBalanceChart', dataLastDayBalanceChart, optionsLastDayBalanceChart);

    md.startAnimationForLineChart(lastDayBalanceChart);

    /* ----------==========     Daily Sales Chart initialization    ==========---------- */

    dataLastHourBalanceChart = {
      labels: dataLastHourBalanceLabels,
      series: [
        dataLastHourSeries
      ]
    };

    optionsLastHourBalanceChart = {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0
      }),
      low: dataLastHourSeriesMin,
      high: dataLastHourSeriesMax, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
      chartPadding: {top: 0, right: 0, bottom: 0, left: 0}
    };

    var lastHourBalanceChart = new Chartist.Line('#lastHourBalanceChart', dataLastHourBalanceChart, optionsLastHourBalanceChart);

    md.startAnimationForLineChart(lastHourBalanceChart);

  }


}
