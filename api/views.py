# Create your views here.
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from api.utils import get_user_from_key
from balances.utils import last_total_balance_value
from balances.views import get_balances_for_exchanges
from charts.rest import last_day_hours_chart, last_hour_chart
from deposits.utils import calculate_deposit


class APIList(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        return Response({

        })


class APIPortfolio(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        currency = request.GET.get('currency', 'EUR')
        current_user = get_user_from_key(request)

        total_balance_quantity, total_balance_money = last_total_balance_value(current_user, currency)
        total_balance = {'quantity': round(total_balance_quantity, 2), 'money': total_balance_money}

        total_deposit_quantity, total_deposit_money = calculate_deposit(current_user, currency)
        total_deposit = {'quantity': round(total_deposit_quantity, 2), 'money': total_balance_money}

        total_gain_quantity = round(total_balance_quantity - total_deposit_quantity, 2)
        total_gain = {'quantity': round(total_gain_quantity, 2), 'money': currency}

        if total_deposit_quantity > 0:
            total_gain_percent = round(total_balance_quantity / total_deposit_quantity * 100, 2)
        else:
            total_gain_percent = None
        return Response({
            'total_balance': total_balance,
            'total_deposit': total_deposit,
            'total_gain': total_gain,
            'total_gain_percent': total_gain_percent,
        })


class APILastDayBalance(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        current_user = get_user_from_key(request)
        last_day_balance_data = last_day_hours_chart(current_user)

        return Response(last_day_balance_data)


class APILastHourBalance(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        current_user = get_user_from_key(request)
        last_hour_balance_data = last_hour_chart(current_user)

        return Response(last_hour_balance_data)


class APIExchangesData(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        currency = request.GET.get('currency', 'EUR')
        current_user = get_user_from_key(request)
        exchanges_data = get_balances_for_exchanges(current_user, fiat_money=currency)

        return Response(exchanges_data)
