from accounts.models import User


def get_user_from_key(request):
    try:
        key = request.GET.get('key', 'A caso, dato che null corrisponde a matteo :D')
        current_user = User.objects.get(activation_token__token=key)
    except User.DoesNotExist:
        current_user = request.user
    return current_user
