from django.conf.urls import url
from django.urls import path

from .views import APIList, APIPortfolio, APILastDayBalance, APILastHourBalance, APIExchangesData

urlpatterns = [
    url(r'^$', APIList.as_view()),
    path('portfolio', APIPortfolio.as_view()),
    path('charts/day', APILastDayBalance.as_view()),
    path('charts/hour', APILastHourBalance.as_view()),
    path('exchange-data', APIExchangesData.as_view()),
]
