import ccxt
from django.conf import settings
from django.core.cache import cache
from django.db import models

from core.models import TimeStampedModel

EXCHANGES = (
    ('bitfinex2', 'bitfinex'),
    ('kraken', 'kraken'),
    # ('coinbase', 'coinbase'),
    ('bitstamp', 'bitstamp'),
    ('binance', 'binance'),
)

SYMBOLS_CACHE = 60 * 60 * 24


class Exchange(TimeStampedModel):
    name = models.CharField(max_length=512)
    code = models.CharField(choices=EXCHANGES, max_length=512)

    def __str__(self):
        return self.name

    def symbols(self):
        cache_key = 'exchange_symbols_{}'.format(self.code)
        symbols = cache.get(cache_key)

        if symbols is None:
            api = getattr(ccxt, self.code)()
            api.load_markets()
            symbols = api.symbols
            cache.set(cache_key, symbols, SYMBOLS_CACHE)
        return symbols


class UserExchange(TimeStampedModel):
    exchange = models.ForeignKey(Exchange, on_delete=models.CASCADE)
    key = models.CharField(max_length=512)
    secret = models.CharField(max_length=512)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return '{} - {}'.format(self.user.email, self.exchange.name)

    def last_balance(self):
        return self.balanceexchangesync_set.order_by('-created')[0]

    def last_balance_value(self, money=settings.DEFAULT_FIAT_MONEY):
        try:
            balance = self.last_balance()
            balance_value, balance_money = balance.value(money, exchange_code=self.exchange.code)
        except IndexError:
            balance_value, balance_money = 0, money

        return balance_value, balance_money

    def last_balance_object(self):
        try:
            balance = self.last_balance()
            balance_object = balance.object()
        except IndexError:
            balance_object = {}

        return balance_object
