from django.contrib import admin

from exchanges.models import UserExchange, Exchange


class UserExchangeAdmin(admin.ModelAdmin):
    pass


class ExchangeAdmin(admin.ModelAdmin):
    pass


admin.site.register(Exchange, ExchangeAdmin)
admin.site.register(UserExchange, UserExchangeAdmin)
