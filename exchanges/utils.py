import ccxt
from ccxt import NotSupported, RequestTimeout, ExchangeNotAvailable
from decimal import Decimal
from django.conf import settings
from django.core.cache import cache

from core.utils import FIAT_MONEYS, fiat_money_converter
from exchanges.exceptions import MoneyCalculatorException
from exchanges.models import EXCHANGES

API_CACHE_TIME = 60 * 4
MONEY_USD_CODE = 'USD'


class MoneyCalculator:
    tickers = {}
    exchange_api = {}
    exchanges = EXCHANGES

    def __init__(self):
        for exchange in self.exchanges:
            exchange_code = exchange[0]
            api = getattr(ccxt, exchange_code)()
            self.exchange_api[exchange_code] = api
            try:
                self._sync_ticker(exchange_code)
            except NotSupported:
                print('{} NOT SUPPORTED!!!!'.format(exchange_code))
            except RequestTimeout:
                print('{} TIMEOUT!!!!'.format(exchange_code))
            except ExchangeNotAvailable:
                print('{} TIMEOUT!!!!'.format(exchange_code))
            except TypeError:
                print('{} TYPEERROR!!!!'.format(exchange_code))

    def _sync_ticker(self, exchange_code):
        cache_key = 'sync_tickers_{}'.format(exchange_code)
        tickers = cache.get(cache_key)
        # tickers = None
        if tickers is None:
            api = self.exchange_api[exchange_code]
            try:
                tickers = api.fetch_tickers()
                cache.set(cache_key, tickers, API_CACHE_TIME)
            except NotSupported:
                print('{} NOT SUPPORTED!!!!'.format(exchange_code))
                tickers = None
            except RequestTimeout:
                print('{} TIMEOUT!!!!'.format(exchange_code))
                tickers = None
            except ExchangeNotAvailable:
                print('{} TIMEOUT!!!!'.format(exchange_code))
                tickers = None
        self.tickers[exchange_code] = tickers

    def get_ticker(self, money_from, money_to, exchange_code=None):
        money_from = money_from.upper()
        money_to = money_to.upper()
        if exchange_code is None:
            if money_from in ['XRP', 'REP']:
                exchange_code = 'kraken'
            else:
                exchange_code = 'bitfinex2'

        self._sync_ticker(exchange_code)
        money_code = '{}/{}'.format(money_from, money_to)
        result = self.tickers[exchange_code][money_code]
        return result

    def search_ticker_in_first_exchange_available(self, money_from, money_to, except_in_exchange_code=None):
        result = None
        for exchange_code in settings.EXCHANGE_CODES:
            if exchange_code != except_in_exchange_code:
                try:
                    result = self.get_ticker(money_from, money_to, exchange_code)
                    break
                except (KeyError, TypeError):
                    pass

        if result:
            return result
        else:
            raise MoneyCalculatorException(
                'search_ticker_in_first_exchange_available not found for {}/{}'.format(money_from, money_to))

    def get_ticker_last_usd_value(self, money_from, exchange_code=None):
        # always get usd ticker value
        money_to = MONEY_USD_CODE

        try:
            ticker_usd = self.get_ticker(money_from, money_to, exchange_code)
            usd_value = ticker_usd['last']
            if usd_value is None:
                usd_value = ticker_usd['bid']
        except (KeyError, TypeError):

            try:
                ticker_usd = self.search_ticker_in_first_exchange_available(money_from, money_to,
                                                                            except_in_exchange_code=exchange_code)
                usd_value = ticker_usd['last']
                if usd_value is None:
                    usd_value = ticker_usd['bid']

            except MoneyCalculatorException:
                try:
                    # proviamo a convertire in BTC
                    ticker_btc = self.search_ticker_in_first_exchange_available(money_from, 'BTC')
                    btc_value = ticker_btc['last']
                    if btc_value is None:
                        btc_value = ticker_btc['bid']
                    usd_value, money = get_current_value(btc_value, 'BTC', 'USD')
                except MoneyCalculatorException:
                    try:
                        # proviamo a convertire in EUR
                        ticker_eur = self.search_ticker_in_first_exchange_available(money_from, 'EUR')
                        eur_value = ticker_eur['last']
                        if eur_value is None:
                            eur_value = ticker_eur['bid']
                        usd_value, money = get_current_value(eur_value, 'EUR', 'USD')
                    except MoneyCalculatorException:
                        print('CANT CONVERT {} in any of BTC USD EUR'.format(money_from))
                        usd_value = 0

        return usd_value, MONEY_USD_CODE


MoneyCalculatorSingleton = MoneyCalculator()


def init_exchange_api(user):
    user_exchanges = user.userexchange_set.all()

    exchanges_objects = []

    for user_exchange in user_exchanges:
        user_exchange_api_instanzied = getattr(ccxt, user_exchange.exchange.code)({
            'apiKey': user_exchange.key,
            'secret': user_exchange.secret,
        })
        exchanges_objects.append({'user_exchange_api': user_exchange_api_instanzied, 'user_exchange': user_exchange})
    return exchanges_objects


def get_current_usd_value(money_from, exchange_code=None):
    result, money_to = MoneyCalculatorSingleton.get_ticker_last_usd_value(money_from, exchange_code=exchange_code)
    return result, money_to


def get_current_value_rate(money_from, money_to, exchange_code=None):
    # TODO: validatione per le monte che possiamo gestire
    money_from = money_from.upper()
    money_to = money_to.upper()

    if money_from in FIAT_MONEYS and money_to in FIAT_MONEYS:
        return fiat_money_converter(1, money_from, money_to)

    elif money_to in FIAT_MONEYS:
        usd_value, money = get_current_usd_value(money_from, exchange_code=exchange_code)
        return fiat_money_converter(usd_value, money, money_to)


def get_current_value(value_from, money_from, money_to, exchange_code=None):
    # TODO: validatione per le monte che possiamo gestire

    rate = get_current_value_rate(money_from, money_to, exchange_code=exchange_code)
    value_to = Decimal(value_from) * Decimal(rate[0])
    return value_to, money_to
