from django.urls import path

from .views import UserExchangeListView, UserExchangeCreate, UserExchangeUpdate, UserExchangeDelete

urlpatterns = [
    path('user-exchanges/', UserExchangeListView.as_view(), name='user_exchange-list'),
    path('user-exchanges/create/', UserExchangeCreate.as_view(), name='user_exchange-create'),
    path('user-exchanges/<int:pk>/update/', UserExchangeUpdate.as_view(), name='user_exchange-update'),
    path('user-exchanges/<int:pk>/delete/', UserExchangeDelete.as_view(), name='user_exchange-delete'),
]
