from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from exchanges.models import UserExchange


class UserExchangeListView(LoginRequiredMixin, ListView):
    model = UserExchange

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return UserExchange.objects.filter(user=self.request.user)


class UserExchangeCreate(LoginRequiredMixin, CreateView):
    model = UserExchange
    fields = ['exchange', 'key', 'secret']
    success_url = reverse_lazy('user_exchange-list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class UserExchangeUpdate(LoginRequiredMixin, UpdateView):
    model = UserExchange
    fields = ['exchange', 'key', 'secret']
    success_url = reverse_lazy('user_exchange-list')

    def get_queryset(self):
        return UserExchange.objects.filter(user=self.request.user)


class UserExchangeDelete(LoginRequiredMixin, DeleteView):
    model = UserExchange
    success_url = reverse_lazy('user_exchange-list')

    def get_queryset(self):
        return UserExchange.objects.filter(user=self.request.user)
