from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from core.models import TimeStampedModel
from .managers import UserManager


class ActivationToken(TimeStampedModel):
    token = models.CharField(max_length=512)
    used = models.BooleanField(default=False)
    date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.token)

    def registration_url(self):
        if not self.used:
            return 'https://crypto.waydotnet.com{}?activationToken={}'.format(reverse('register'), self.token)
        else:
            return ''

    def registration_link(self):
        url = self.registration_url()
        return mark_safe("<a href='{}'>{}</a>".format(url, url))


class User(AbstractUser):
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    username = None

    email = models.EmailField(_('email address'), unique=True)
    activation_token = models.ForeignKey(ActivationToken, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.email)
