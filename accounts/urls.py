from django.contrib.auth.views import login, logout
from django.urls import path

from .views import sign_up, waitlist, profile, change_password, UpdateProfile

urlpatterns = [
    path('profile/update/', UpdateProfile.as_view(), name='update_profile'),
    path('profile/change-password/', change_password, name='change_password'),
    path('signup/', sign_up, name='register'),
    path('signin/', login, {'template_name': 'accounts/login.html'}, name='login'),
    path('logout/', logout, name='logout'),
    path('waitlist/', waitlist, name='waitlist'),
]
