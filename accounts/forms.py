from django import forms
from django.contrib.auth.forms import UserCreationForm, UsernameField, UserChangeForm
from django.utils.translation import gettext_lazy as _

from accounts.models import User


class RegistrationForm(UserCreationForm):
    token = forms.CharField(
        label=_("Invite Token"),
        strip=True,
        help_text=_('Paste here the invitation token if you have one or you will be in waiting list.'),
        required=False,
    )

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "password1", "password2",)
        field_classes = {'username': UsernameField}

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class UpdateProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("first_name", "last_name", "email")
