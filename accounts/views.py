import datetime

from django.contrib import messages
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import UpdateView

from accounts.forms import RegistrationForm, UpdateProfileForm
from accounts.models import ActivationToken, User


def sign_up(request):
    token = request.GET.get('activationToken')

    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            token = form.cleaned_data.get('token')

            try:
                token = ActivationToken.objects.get(used=False, token=token)
                token.used = True
                token.date = datetime.datetime.now()
                user = authenticate(username=email, password=raw_password)
                user.activation_token = token

                token.save()
                user.save()

                login(request, user)
                return redirect('home')
            except ActivationToken.DoesNotExist:
                return redirect('waitlist')
    else:
        initial_data = {}
        if token:
            initial_data['token'] = token
        form = RegistrationForm(initial=initial_data)
    return render(request, 'accounts/register.html', {'form': form})


class UpdateProfile(LoginRequiredMixin, UpdateView):
    template_name = 'accounts/update_profile.html'
    form_class = UpdateProfileForm
    model = User
    success_url = reverse_lazy('update_profile')

    def get_object(self, queryset=None):
        '''This loads the profile of the currently logged in user'''

        return self.request.user

        # def form_valid(self, form):
        #     '''Here is where you set the user for the new profile'''
        #
        #     instance = form.instance  # This is the new object being saved
        #     instance.user = self.request.user
        #     instance.save()
        #
        # return super(UpdateProfile, self).form_valid(form)


@login_required
def profile(request):
    if request.method == 'POST':
        form = UpdateProfileForm(request.user, request.POST)
        if form.is_valid():
            form.save()
    else:
        form = UpdateProfileForm(instance=request.user)
    return render(request, 'accounts/update_profile.html', {'form': form})


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, _('Your password was successfully updated!'))
            return redirect('update_profile')
        else:
            messages.error(request, _('Please correct the error below.'))
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'accounts/change_password.html', {
        'form': form
    })


def waitlist(request):
    return render(request, 'accounts/waitlist.html', {})
