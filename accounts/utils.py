import uuid

from accounts.models import ActivationToken


def create_activation_token(number):
    for i in range(number):
        ActivationToken.objects.create(token=uuid.uuid4())
