from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import ActivationToken, User


class UserAdmin(admin.ModelAdmin):
    pass


class ActivationTokenAdmin(admin.ModelAdmin):
    list_display = [
        'token',
        'used',
        'date',
        'registration_link',
    ]
    list_filter = [
        'used'
    ]
    readonly_fields = ['registration_link']

    @staticmethod
    def registration_link(instance):
        result = mark_safe(instance.registration_link())
        return result


admin.site.register(ActivationToken, ActivationTokenAdmin)
admin.site.register(User, UserAdmin)
