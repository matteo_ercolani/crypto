import json
from datetime import datetime, timedelta

from django.core.cache import cache

from balances.models import UserBalanceLog
from charts.utils import hours_range_datetime_list


def last_day_hours_chart(user):
    cache_key = 'last_day_hours_chart_user_{}'.format(user.pk)
    results = cache.get(cache_key)
    if results is None:
        hours_list = hours_range_datetime_list(24)

        hours = []
        data = []

        for i, hour in enumerate(hours_list):
            hour_max = hour.replace(minute=59, second=59, microsecond=0)
            hour_min = hour.replace(minute=0, second=0, microsecond=0)

            logs = UserBalanceLog.objects.filter(user=user, created__range=[hour_min, hour_max]).order_by('created')
            try:
                hour_value = float(round(logs[0].quantity, 2))
            except IndexError:
                continue

            hours.append(hour_max.strftime('at %I %p'))

            data.append(hour_value)

        results = {
            'labels': json.dumps(hours),
            'series': json.dumps(data),
            'min': min(data) if data else 0,
            'max': max(data) if data else 0,
            'money': 'EUR'
        }
        cache.set(cache_key, results, 59 * 60)
    return results


def last_hour_chart(user):
    cache_key = 'last_hour_chart_user_{}'.format(user.pk)
    results = cache.get(cache_key)
    if results is None:

        hours = []
        data = []

        max_date = datetime.now()
        min_date = max_date - timedelta(hours=1)
        logs = UserBalanceLog.objects.filter(user=user, created__range=[min_date, max_date]).order_by('created')

        for i, log in enumerate(logs):
            try:
                hour_value = float(round(log.quantity, 2))
            except IndexError:
                continue
            hours.append(log.created.strftime('at %H:%-M'))
            data.append(hour_value)

        results = {
            'labels': json.dumps(hours),
            'series': json.dumps(data),
            'min': min(data) if data else 0,
            'max': max(data) if data else 0,
            'money': 'EUR'
        }
        cache.set(cache_key, results, 4 * 60)
    return results
