from datetime import timedelta, datetime


def hours_range_datetime_list(number_of_hours):
    result = []

    now = datetime.now()

    for i in range(number_of_hours):
        hour = timedelta(hours=i)
        result.append(now - hour)

    return reversed(result)
