from django.apps import AppConfig


class MoneyConvertersConfig(AppConfig):
    name = 'money_converters'
